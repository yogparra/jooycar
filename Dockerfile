FROM node:alpine as build
WORKDIR /usr/src/app
COPY package*.json ./
COPY tsconfig.json ./
COPY .env ./
COPY src/ src/
RUN npm i
RUN npm run build
RUN npm i pm2 -g

EXPOSE 8080

CMD [ "pm2-runtime", "dist/index.js" ]