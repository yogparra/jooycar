compose-up:
	docker-compose -f docker-compose.yml up -d

database-insert:
	docker exec mongo-typescript bash -c './database/import.sh localhost'

app-challenge-up:
	make compose-up
	make database-insert

app-challenge-restart:
	docker stop typescript-app
	docker rm typescript-app
	docker rmi typescript-app
	make compose-up

app-challenge-down:
	docker stop typescript-app
	docker rm typescript-app
	docker rmi typescript-app
	docker stop mongo-typescript
	docker rm mongo-typescript
	docker rmi mongo:4.2.6-bionic

down-mongo:
	docker stop mongo-typescript
	docker rm mongo-typescript
	docker rmi mongo:4.2.6-bionic