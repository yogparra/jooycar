import { Request, Response } from 'express';
import { Reading, Trips } from '../interfaces/interfaces';
import { calculateDistance, getAddressFromCoordinates } from '../utils/utils';
import { collection } from '../db/db';

export const getTrips = async (req: Request, res: Response) => {

  const startGte: string | undefined = req.query.start_gte?.toString();
  const endGte: string | undefined = req.query.end_gte?.toString();
  const distanceGte: string | undefined = req.query.distance_gte?.toString();

  const limit: number = parseInt(req.query.limit as string) || 20;
  const offset: number = parseInt(req.query.offset as string) || 0;

  try {

    const filters: any = {};
    if (startGte) {
      filters['start.time'] = { $gte: new Date(startGte) };
    }
    if (endGte) {
      filters['end.time'] = { $gte: new Date(endGte) };
    }
    if (distanceGte) {
      filters['distance'] = { $gte: parseFloat(distanceGte) };
    }

    const trips: Trips[] = await collection
      .find(filters)
      .limit(limit)
      .skip(offset)
      .toArray();

    res.json({ trips });

  } catch (error) {
    console.error('Error al conectar a MongoDB:', error);
    res.status(500).json({ error: 'Error al obtener los trips de la base de datos' });
  }
}

export const postTrips = async (req: Request, res: Response) => {

  const readings: Reading[] = req.body.readings;

  if (readings.length < 5) {    
    res.status(400).json({
      error: {
        statusCode: 400,
        errorCode: 0,
        srcMessage: 'There must be at least 5 readings',
        translatedMessage: 'Debe haber al menos 5 readings'
      }
    })
    return;
  }

  if (readings.some((reading) => typeof reading.time !== 'number')) {    
    res.status(400).json({
      error: {
        statusCode: 400,
        errorCode: 0,
        srcMessage: 'All readings must have the time property',
        translatedMessage: 'Todos los readings deben tener la propiedad time'
      }
    })
    return;
  }

  const startReading: Reading = readings.reduce(
    (minReading: Reading, currentReading: Reading) => {
      if (currentReading.time < minReading.time) {
        return currentReading;
      } else {
        return minReading;
      }
    }
  );

  const endReading: Reading = readings.reduce(
    (maxReading: Reading, currentReading: Reading) => {
      if (currentReading.time > maxReading.time) {
        return currentReading;
      } else {
        return maxReading;
      }
    }
  );

  const duration: number = endReading.time - startReading.time;

  const distance: number = readings.reduce((totalDistance: number, currentReading: Reading, index: number) => {
    if (index > 0) {
      const prevReading = readings[index - 1];
      const distanceBetweenReadings: number = calculateDistance(
        prevReading.location.lat,
        prevReading.location.lon,
        currentReading.location.lat,
        currentReading.location.lon
      );
      return totalDistance + distanceBetweenReadings;
    } else {
      return totalDistance;
    }
  }, 0);

  let overspeedsCount: number = 0;
  let isOverspeedSegment: boolean = false;

  readings.forEach((reading: Reading, index: number) => {
    if (index > 0) {
      const prevReading = readings[index - 1];
      if (reading.speed > reading.speedLimit) {
        if (!isOverspeedSegment) {
          overspeedsCount++;
          isOverspeedSegment = true;
        }
      } else {
        isOverspeedSegment = false;
      }
    }
  });

  const startAddress: string = await getAddressFromCoordinates(
    startReading.location.lat,
    startReading.location.lon
  );

  const endAddress: string = await getAddressFromCoordinates(
    endReading.location.lat,
    endReading.location.lon
  );

  // Construye el objeto trip con los datos obtenidos
  const trips: Trips = {
    start: {
      time: startReading.time,
      lat: startReading.location.lat,
      lon: startReading.location.lon,
      address: startAddress,
    },
    end: {
      time: endReading.time,
      lat: endReading.location.lat,
      lon: endReading.location.lon,
      address: endAddress,
    },
    duration: duration,
    distance: distance,
    overspeedsCount: overspeedsCount,
    boundingBox: readings.map((reading: Reading) => {
      return { lat: reading.location.lat, lon: reading.location.lon };
    }),
  };

  try {

    collection.insertOne(trips)
    res.json({ trips });

  } catch (error) {    
    res.status(500).json({
      error: {
        statusCode: 500,
        errorCode: 0,
        srcMessage: 'Error saving the trip in the database',
        translatedMessage: 'Error al guardar el trip en la base de datos'
      }
    })
  }
}

export const postDemo = async (req: Request, res: Response) => {

  const { body } = req;

  res.json({
    msg: 'postTripsDemo',
    body
  })

  /*
  res.status(500).json({    
    error: {
      statusCode: 500,
      errorCode: 0,
      srcMessage: 'Error al guardar el trip en la base de datos',
      translatedMessage: 'Atributo inválido'
    }
  })
  */

}