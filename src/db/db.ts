import { MongoClient, Collection } from 'mongodb';
import { Trips } from '../interfaces/interfaces';

let collection: Collection<Trips>;
let mongodb = process.env.MONGODB || 'mongodb://localhost:27017/'

async function connectDB() {
    try {    
        
        const connection = await MongoClient.connect(mongodb);    
        collection = connection.db("jooycar").collection("trips");

        console.log('Conectado a MongoDB');
    } catch (error) {
         console.error('Error al conectar a MongoDB:', error);
    }
}

export { connectDB, collection };