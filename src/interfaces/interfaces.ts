
interface Reading {
    time: number;
    location: {
        lat: number;
        lon: number;
    };
    speed: number;
    speedLimit: number;
}

interface Trips {
    start: {
        time: number;
        lat: number;
        lon: number;
        address: string;
    };
    end: {
        time: number;
        lat: number;
        lon: number;
        address: string;
    };
    duration: number;
    distance: number;
    overspeedsCount: number;
    boundingBox: { lat: number; lon: number }[];
}

interface AddressComponents {    
    road?: string;
    neighbourhood?: string;
    suburb?: string;
    city?: string;
    county?: string;
    state?: string;
    country?: string;
}

export { Reading, Trips, AddressComponents };