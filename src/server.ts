import express, { Application } from 'express';
import router from './routes/trips';
import cors from 'cors';
import { connectDB } from './db/db';

class Server {

    private app: Application;
    private port: string;
    private paths = {
        trips: '/api/trips'
    }

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8080';

        this.dbConnection();
        this.middleware();
        this.routes();
    }

    dbConnection() {
        connectDB();
    }

    middleware() {
        this.app.use(cors());
        this.app.use(express.json());
    }

    routes() {
        this.app.use(this.paths.trips, router)
    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        })
    }
}

export default Server;