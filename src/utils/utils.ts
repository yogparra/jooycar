
import { AddressComponents } from '../interfaces/interfaces';

const calculateDistance = (lat1: number, lon1: number, lat2: number, lon2: number): number => {
  const R = 6371; // Radio de la Tierra en kilómetros
  const dLat = deg2rad(lat2 - lat1);
  const dLon = deg2rad(lon2 - lon1);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = R * c;

  return distance;
};

const deg2rad = (deg: number): number => {
  return deg * (Math.PI / 180);
};

const getAddressFromCoordinates = async (lat: number, lon: number): Promise<string> => {

  const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lon}`;

  try {
    const response = await fetch(url);
    const data = await response.json();

    if (data.address) {
      const components: AddressComponents = data.address;
      const addressParts: string[] = [];

      if (components.road) addressParts.push(components.road);
      if (components.neighbourhood) addressParts.push(components.neighbourhood);
      if (components.suburb) addressParts.push(components.suburb);
      if (components.city) addressParts.push(components.city);
      if (components.county) addressParts.push(components.county);
      if (components.state) addressParts.push(components.state);
      if (components.country) addressParts.push(components.country);
      return addressParts.join(', ');

    } else {
      throw new Error('No se pudo obtener la dirección.');
    }
  } catch (error) {
    throw new Error('Error en la solicitud de geocodificación');
  }

};

export { calculateDistance, getAddressFromCoordinates };