import { Router } from 'express';
import { getTrips, postTrips, postDemo } from '../controllers/trips';

const router = Router();

router.get('/v1',       getTrips );
router.post('/v1',      postTrips );

//pruebas body
router.post('/v2',      postDemo );


export default router;