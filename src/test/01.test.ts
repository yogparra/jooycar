import { postTrips } from "../controllers/trips";

describe('PostTrips Caso1', () => {

    let req: any;
    let res: any;

    beforeEach(() => {
        req = {
            body: {
                readings: [
                    {time:1,location:{lat:0,lon:0},speed:50,speedLimit:50},
                    {time:2,location:{lat:0,lon:0},speed:60,speedLimit:60},
                    {time:3,location:{lat:0,lon:0},speed:70,speedLimit:70},
                    {time:4,location:{lat:0,lon:0},speed:80,speedLimit:80},                    
                ],
            },
        };
        res = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn(),
        };
    });

    it('Devuelve error si hay 4 o menos readings', () => {
        postTrips(req, res);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            error: {
                statusCode: 400,
                errorCode: 0,
                srcMessage: 'There must be at least 5 readings',
                translatedMessage: 'Debe haber al menos 5 readings',
            },
        });
    });
});