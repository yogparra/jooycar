# Jooycar

## node / typescript
```
  node --version
  v18.15.0
  tsc --version 
  Version 5.1.3
```

## iniciando el proyecto
```
npm init -y
tsc --init
```

## tip
```
  tsc 
  node dist/index.js  
  npx ts-node src/index.ts  
```

## cliente rest insomnia
```
  get   :localhost:8080/api/trips/v1
  post  :localhost:8080/api/trips/v1
```

## ejecutando la aplicacion docker
```    
  make app-challenge-up
  make app-challenge-restart
  make app-challenge-down
```

## comandos make
```
  revisar el archivo Makefile
```

## cliente mongoDB mongoDB for vscode
```
  localhost:2717  
```

## test
```
  npx jest src/test/01.test.ts
```

## Imagenes

### ejemplos get
![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/get_ok-1.png)

![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/get_ok-2.png)

### ejemplos post
![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/post_ok-1.png)

![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/post_ok-2.png)

### add-adress
![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/post_ok-add-adress.png)

![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/post_bad-1.png)

![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/post_bad-1.png)

### ejemplos bd
![](https://gitlab.com/yogparra/jooycar/-/raw/dev/public/img/bd_ok.png)

## Personal
```
  Muchas gracias :)  
```