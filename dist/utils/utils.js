"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAddressFromCoordinates = exports.calculateDistance = void 0;
const calculateDistance = (lat1, lon1, lat2, lon2) => {
    const R = 6371; // Radio de la Tierra en kilómetros
    const dLat = deg2rad(lat2 - lat1);
    const dLon = deg2rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = R * c;
    return distance;
};
exports.calculateDistance = calculateDistance;
const deg2rad = (deg) => {
    return deg * (Math.PI / 180);
};
const getAddressFromCoordinates = (lat, lon) => __awaiter(void 0, void 0, void 0, function* () {
    const url = `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${lat}&lon=${lon}`;
    try {
        const response = yield fetch(url);
        const data = yield response.json();
        if (data.address) {
            const components = data.address;
            const addressParts = [];
            if (components.road)
                addressParts.push(components.road);
            if (components.neighbourhood)
                addressParts.push(components.neighbourhood);
            if (components.suburb)
                addressParts.push(components.suburb);
            if (components.city)
                addressParts.push(components.city);
            if (components.county)
                addressParts.push(components.county);
            if (components.state)
                addressParts.push(components.state);
            if (components.country)
                addressParts.push(components.country);
            return addressParts.join(', ');
        }
        else {
            throw new Error('No se pudo obtener la dirección.');
        }
    }
    catch (error) {
        throw new Error('Error en la solicitud de geocodificación');
    }
});
exports.getAddressFromCoordinates = getAddressFromCoordinates;
