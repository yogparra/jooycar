"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postDemo = exports.postTrips = exports.getTrips = void 0;
const utils_1 = require("../utils/utils");
const db_1 = require("../db/db");
const getTrips = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c;
    const startGte = (_a = req.query.start_gte) === null || _a === void 0 ? void 0 : _a.toString();
    const endGte = (_b = req.query.end_gte) === null || _b === void 0 ? void 0 : _b.toString();
    const distanceGte = (_c = req.query.distance_gte) === null || _c === void 0 ? void 0 : _c.toString();
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    try {
        const filters = {};
        if (startGte) {
            filters['start.time'] = { $gte: new Date(startGte) };
        }
        if (endGte) {
            filters['end.time'] = { $gte: new Date(endGte) };
        }
        if (distanceGte) {
            filters['distance'] = { $gte: parseFloat(distanceGte) };
        }
        const trips = yield db_1.collection
            .find(filters)
            .limit(limit)
            .skip(offset)
            .toArray();
        res.json({ trips });
    }
    catch (error) {
        console.error('Error al conectar a MongoDB:', error);
        res.status(500).json({ error: 'Error al obtener los trips de la base de datos' });
    }
});
exports.getTrips = getTrips;
const postTrips = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const readings = req.body.readings;
    if (readings.length < 5) {
        res.status(400).json({
            error: {
                statusCode: 400,
                errorCode: 0,
                srcMessage: 'There must be at least 5 readings',
                translatedMessage: 'Debe haber al menos 5 readings'
            }
        });
        return;
    }
    if (readings.some((reading) => typeof reading.time !== 'number')) {
        res.status(400).json({
            error: {
                statusCode: 400,
                errorCode: 0,
                srcMessage: 'All readings must have the time property',
                translatedMessage: 'Todos los readings deben tener la propiedad time'
            }
        });
        return;
    }
    const startReading = readings.reduce((minReading, currentReading) => {
        if (currentReading.time < minReading.time) {
            return currentReading;
        }
        else {
            return minReading;
        }
    });
    const endReading = readings.reduce((maxReading, currentReading) => {
        if (currentReading.time > maxReading.time) {
            return currentReading;
        }
        else {
            return maxReading;
        }
    });
    const duration = endReading.time - startReading.time;
    const distance = readings.reduce((totalDistance, currentReading, index) => {
        if (index > 0) {
            const prevReading = readings[index - 1];
            const distanceBetweenReadings = (0, utils_1.calculateDistance)(prevReading.location.lat, prevReading.location.lon, currentReading.location.lat, currentReading.location.lon);
            return totalDistance + distanceBetweenReadings;
        }
        else {
            return totalDistance;
        }
    }, 0);
    let overspeedsCount = 0;
    let isOverspeedSegment = false;
    readings.forEach((reading, index) => {
        if (index > 0) {
            const prevReading = readings[index - 1];
            if (reading.speed > reading.speedLimit) {
                if (!isOverspeedSegment) {
                    overspeedsCount++;
                    isOverspeedSegment = true;
                }
            }
            else {
                isOverspeedSegment = false;
            }
        }
    });
    const startAddress = yield (0, utils_1.getAddressFromCoordinates)(startReading.location.lat, startReading.location.lon);
    const endAddress = yield (0, utils_1.getAddressFromCoordinates)(endReading.location.lat, endReading.location.lon);
    // Construye el objeto trip con los datos obtenidos
    const trips = {
        start: {
            time: startReading.time,
            lat: startReading.location.lat,
            lon: startReading.location.lon,
            address: startAddress,
        },
        end: {
            time: endReading.time,
            lat: endReading.location.lat,
            lon: endReading.location.lon,
            address: endAddress,
        },
        duration: duration,
        distance: distance,
        overspeedsCount: overspeedsCount,
        boundingBox: readings.map((reading) => {
            return { lat: reading.location.lat, lon: reading.location.lon };
        }),
    };
    try {
        db_1.collection.insertOne(trips);
        res.json({ trips });
    }
    catch (error) {
        res.status(500).json({
            error: {
                statusCode: 500,
                errorCode: 0,
                srcMessage: 'Error saving the trip in the database',
                translatedMessage: 'Error al guardar el trip en la base de datos'
            }
        });
    }
});
exports.postTrips = postTrips;
const postDemo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    res.json({
        msg: 'postTripsDemo',
        body
    });
    /*
    res.status(500).json({
      error: {
        statusCode: 500,
        errorCode: 0,
        srcMessage: 'Error al guardar el trip en la base de datos',
        translatedMessage: 'Atributo inválido'
      }
    })
    */
});
exports.postDemo = postDemo;
