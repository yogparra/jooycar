"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const trips_1 = __importDefault(require("./routes/trips"));
const cors_1 = __importDefault(require("cors"));
const db_1 = require("./db/db");
class Server {
    constructor() {
        this.paths = {
            trips: '/api/trips'
        };
        this.app = (0, express_1.default)();
        this.port = process.env.PORT || '8080';
        this.dbConnection();
        this.middleware();
        this.routes();
    }
    dbConnection() {
        (0, db_1.connectDB)();
    }
    middleware() {
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.json());
    }
    routes() {
        this.app.use(this.paths.trips, trips_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port);
        });
    }
}
exports.default = Server;
