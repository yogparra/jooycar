"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const trips_1 = require("../controllers/trips");
const router = (0, express_1.Router)();
router.get('/v1', trips_1.getTrips);
router.post('/v1', trips_1.postTrips);
//pruebas body
router.post('/v2', trips_1.postDemo);
exports.default = router;
